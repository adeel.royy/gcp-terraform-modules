
terraform {
  backend "remote" {}
}

data "terraform_remote_state" "project" {
  backend = "remote"

  config = {
    hostname     = "${var.tfe_hostname}"
    organization = "${var.tfe_organization}"

    workspaces = {
      name = "${var.tfe_project_workspace}"
    }
  }
}

# Secrets and credentials come from Vault
provider "vault" {
  version = "2.22.1"

  auth_login = {
    path = "auth/approle/login"

    parameters = {
      role_id   = "${var.vault_approle}"
      secret_id = "${var.vault_approle_secret}"
    }
  }
}

# Read credentials for the resource editor
# Ensure the path matches:
# - the GCP folder: non-prod, prod, proving or shared
# - the application: example here
# - the environment: dev1, uat4, prd
data "vault_generic_secret" "resource_editor" {
  path = "${local.folder}/gcp/token/${local.env}-modules-editor"
}

provider "google" {
  version      = "3.71.0"
  access_token = data.vault_generic_secret.resource_editor.data["token"]
  region       = "europe-west2"
  project      = data.terraform_remote_state.project.project_id
}

provider "google-beta" {
  version      = "3.71.0"
  access_token = data.vault_generic_secret.resource_editor.data["token"]
  region       = "europe-west2"
  project      = data.terraform_remote_state.project.project_id
}

locals {
  region      = "europe-west2"
  project_id  = data.terraform_remote_state.project.outputs.project_id
  env         = data.terraform_remote_state.project.outputs.env
  folder      = data.terraform_remote_state.project.outputs.project_folder_name
  application = data.terraform_remote_state.project.outputs.application
}

module "default" {
  source                 = "https://gitlab.platform.nwminfra.net/tfe-shared-modules/terraform-gcp-gke-cluster.git?ref=v0.1.0"
  env                    = "prv"
  cmdb_id                = "123456"
  application            = "cluster"
  owner                  = "dave"
  cost_center            = "cost-centre"
  tfe_organization       = "nwm-proving"
  tfe_project_workspace  = "prv-cluster-project"
  cluster_ip_range_name  = "shd-private1-sn-prv-pods"
  services_ip_range_name = "shd-private1-sn-prv-services"
  master_ipv4_cidr_block = "192.168.1.0/28"
  data_classification    = "internal"
  max_pods_per_node      = 16
  flux_suspend           = "false"
  access_token           = data.vault_generic_secret.resource_editor.data["token"]
  shared_vpc_project_id  = data.terraform_remote_state.project.outputs.shared_vpc_project_id
  shared_vpc_subnet      = "shd-private1-npd1-euw2"
}
