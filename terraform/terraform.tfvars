project_id             = "gcp-bigdata-demo-1"
gke_cluster_name       = "gke-bigdata-demo"
gke_machine_type       = "n1-standard-1"
region                 = "us-west1"
gke_initial_node_count = 2
dataset_id  = "foo"
dataset_name = "bar"
expiration = 3600000
dataset_labels = {
  env   = "dev"
  billable   = "true"
  owner = "janesmith"
}
time_partitioning = "DAY"
tables = [
  {
    table_id = "foo",
    schema = "sample_bq_schema.json",
    labels = {
      env = "dev"
      billable = "true"
      owner = "joedoe"
    },
  },
]
