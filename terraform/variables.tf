variable "project_id" {
  type = string
}

variable "gke_cluster_name" {
  type = string
}

variable "gke_machine_type" {
  type = string
}

variable "region" {
  type = string
}

variable "gke_initial_node_count" {
  type    = number
  default = 2
}

variable "expiration" {
  description = "TTL of tables using the dataset in MS"
}


variable "time_partitioning" {
  description = "Configures time-based partitioning for this table"
}

variable "dataset_labels" {
  description = "A mapping of labels to assign to the table"
  type        = map(string)
}

variable "dataset_id" {
  description = "A mapping of labels to assign to the table"
  type        = string
}

variable "dataset_name" {
  description = "A mapping of labels to assign to the table"
  type        = string
}

variable "tables" {
  description = "A list of maps that includes both table_id and schema in each element, the table(s) will be created on the single dataset"
  default     = []
  type = list(object({
    table_id = string,
    schema   = string,
    labels   = map(string),
  }))
}
