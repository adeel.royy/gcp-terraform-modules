resource "google_redis_instance" "memstore" {
  name                    = var.name
  display_name            = var.display_name
  tier                    = var.tier
  region                  = var.region
  project                 = var.project
  location_id             = var.location_id
  alternative_location_id = var.alternative_location_id

  memory_size_gb          = var.memory_size_gb
  connect_mode            = var.connect_mode
  auth_enabled            = var.auth_enabled
  transit_encryption_mode = var.transit_encryption_mode
  authorized_network      = var.authorized_network

  redis_version           = var.redis_version
  redis_configs           = var.redis_configs
  reserved_ip_range       = var.reserved_ip_range
  labels                  = var.labels
}
