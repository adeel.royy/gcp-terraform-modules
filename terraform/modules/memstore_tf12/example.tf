terraform {
  backend "remote" {}
}

data "terraform_remote_state" "project" {
  backend = "remote"

  config = {
    hostname     = "${var.tfe_hostname}"
    organization = "${var.tfe_organization}"

    workspaces = {
      name = "${var.tfe_project_workspace}"
    }
  }
}

# Secrets and credentials come from Vault
provider "vault" {
  version = "2.22.1"

  auth_login = {
    path = "auth/approle/login"

    parameters = {
      role_id   = "${var.vault_approle}"
      secret_id = "${var.vault_approle_secret}"
    }
  }
}

provider "google" {
  version      = "3.71.0"
  access_token = data.vault_generic_secret.resource_editor.data["token"]
  region       = "europe-west2"
  project      = data.terraform_remote_state.project.project_id
}

provider "google-beta" {
  version      = "3.71.0"
  access_token = data.vault_generic_secret.resource_editor.data["token"]
  region       = "europe-west2"
  project      = data.terraform_remote_state.project.project_id
}

locals {
  region      = "europe-west2"
  project_id  = data.terraform_remote_state.project.outputs.project_id
  env         = data.terraform_remote_state.project.outputs.env
  folder      = data.terraform_remote_state.project.outputs.project_folder_name
  application = data.terraform_remote_state.project.outputs.application
}

# Using the memstore module
module "memstore" {
  source                  = "https://gitlab.platform.nwminfra.net/tfe-shared-modules/terraform-gcp-memorystore.git?ref=v0.1.0"
  redis_memory_size_gb    = 1
  redis_name              = "redis-instance-1"
  redis_version           = "REDIS_4_0"
  redis_display_name      = "redis instance"
  redis_tier              = "STANDARD_HA"
  reserved_ip_range       = "192.168.0.0/29"
  transit_encryption_mode = "SERVER_AUTHENTICATION"
  auth_enabled            = true
  connect_mode            = "PRIVATE_SERVICE_ACCESS"
  redis_configs = {
    "notify-keyspace-events" = "gxE"
    }
  redis_labels = {
    "env"                    = "dev"
    "managed-by"             = "terraform"
    }
}
