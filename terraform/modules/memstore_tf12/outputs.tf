output "id" {
  description = "The memorystore instance ID."
  value       = google_redis_instance.memstore.id
}

output "host" {
  description = "The IP address of the instance."
  value       = google_redis_instance.memstore.host
}

output "port" {
  description = "The port number of the exposed Redis endpoint."
  value       = google_redis_instance.memstore.port
}

output "persistence_iam_identity" {
  description = "Cloud IAM identity used by import/export operations. Format is 'serviceAccount:'."
  value       = google_redis_instance.memstore.persistence_iam_identity
}
