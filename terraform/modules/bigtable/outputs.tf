output "bt_instance_display_name" {
  description = "returns a string"
  value       = google_bigtable_instance.this.display_name
}

output "bt_instance_id" {
  description = "returns a string"
  value       = google_bigtable_instance.this.id
}