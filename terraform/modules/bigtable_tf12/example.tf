terraform {
  backend "remote" {}
}

data "terraform_remote_state" "project" {
  backend = "remote"

  config = {
    hostname     = "${var.tfe_hostname}"
    organization = "${var.tfe_organization}"

    workspaces = {
      name = "${var.tfe_project_workspace}"
    }
  }
}

# Secrets and credentials come from Vault
provider "vault" {
  version = "2.22.1"

  auth_login = {
    path = "auth/approle/login"

    parameters = {
      role_id   = "${var.vault_approle}"
      secret_id = "${var.vault_approle_secret}"
    }
  }
}

provider "google" {
  version      = "3.71.0"
  access_token = data.vault_generic_secret.resource_editor.data["token"]
  region       = "europe-west2"
  project      = data.terraform_remote_state.project.project_id
}

provider "google-beta" {
  version      = "3.71.0"
  access_token = data.vault_generic_secret.resource_editor.data["token"]
  region       = "europe-west2"
  project      = data.terraform_remote_state.project.project_id
}

locals {
  region      = "europe-west2"
  project_id  = data.terraform_remote_state.project.outputs.project_id
  env         = data.terraform_remote_state.project.outputs.env
  folder      = data.terraform_remote_state.project.outputs.project_folder_name
  application = data.terraform_remote_state.project.outputs.application
}

# Using the memstore module
module "bigtable" {
  source = "https://gitlab.platform.nwminfra.net/tfe-shared-modules/terraform-gcp-bigtable.git?ref=v0.1.0"

  bt_instance = [
    {
      name              = "bigtable-instance-1"
      delete_protection = true
      labels = {
        "env"        = "dev"
        "managed-by" = "terraform"
      }
      cluster = [
        {
          cluster_id   = "bigtable-instance-cluster-1"
          storage_type = "SSD"
          zone         = "europe-west2-a"
          num_nodes    = 3
          kms_key_name = ""
        },
        {
          cluster_id   = "bigtable-instance-cluster-2"
          storage_type = "SSD"
          zone         = "europe-west2-b"
          num_nodes    = 3
          kms_key_name = ""
        }
      ]
    }
  ]

  bt_table = [
    {
      instance_name = "bigtable-instance-1"
      name          = "bt-table-1"
      split_keys    = ["KEY_RAND", "ASSET_NO", "N_ID"]
      column_family = [
        { family = "first-name" },
        { family = "last-name" }
      ]
    }
  ]

  bt_app_profile = [
    {
      app_profile_id  = "bt-app-profile-single-cluster"
      instance        = "bigtable-instance-1"
      ignore_warnings = false
      single_cluster_routing = [
        {
          cluster_id                 = "bigtable-instance-cluster-1"
          allow_transactional_writes = "true"
        }
      ]
    }
  ]

  bt_gc_policy = [
    {
      instance_name = "bigtable-instance-1"
      table_name    = "bt-table-1"
      column_family = "family-name"
      mode          = "UNION"
      max_age = [
        {
          days = "60"
        }
      ]
      max_version = [
        {
          number = "200"
        }
      ]
    }
  ]

}
