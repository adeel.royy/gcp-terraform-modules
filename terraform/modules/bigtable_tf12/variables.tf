variable "bt_instance" {
  description = "Google Cloud Bigtable instance with cluster properties."
  type        = list(any)
}

variable "bt_table" {
  description = "Google Cloud Bigtable table inside an instance with table properties."
  type        = list(any)
  default     = []
}

variable "app_profile" {
  description = "Bigtable app profile describing how to treat traffic from a particular end user application."
  type        = list(any)
  default     = []
}

variable "bt_gc_policy" {
  description = "Bigtable GarbageCollection Policy inside a family."
  type        = list(any)
  default     = []
}

variable "bt_iam_policy" {
  description = "IAM policies on bigtable instances."
  type        = list(any)
  default     = []
}