# Terraform GCP Redis Memory Store Module
Terraform module for provisioning GCP Memory Store.

## Usage
For usage details, please navigate to [examples](./examples/memstore).

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| `name` | The ID of the instance or a fully qualified identifier for the instance. | string | - | Yes |
| `display_name` | An arbitrary and optional user-provided name for the instance. | string | - | No |
| `location_id` | The zone where the instance will be provisioned. If not provided, the service will choose a zone for the instance. | string | - | No |
| `alternative_location_id` | The alternative zone where the instance will be provisioned. | string | - | No |
| `tier` | The service tier of the instance. Must be one of these values: BASIC or STANDARD_HA | string | BASIC | No |
| `memory_size_gb` | Redis memory size in GiB. Defaulted to 1 GiB | number | 1 | Yes |
| `redis_version` | The version of Redis software. If not provided, latest supported version will be used.  | string | - | No |
| `redis_configs` | This default setting is necessary for gate to work with hosted redis services like memorystore. | map | {} | No |
| `reserved_ip_range` | The CIDR range of internal addresses that are reserved for this instance. | string | - | No |
| `authorized_network` | The networks (self-link) that can connect to the memorystore instance. | string | - | No |
| `connect_mode` | The connection mode of the Redis instance. Can be either DIRECT_PEERING or PRIVATE_SERVICE_ACCESS. | string | - | No |
| `auth_enabled` | Indicates whether OSS Redis AUTH is enabled for the instance. If set to true AUTH is enabled on the instance. | bool | - | No |
| `transit_encryption_mode` | The TLS mode of the Redis instance, If not provided, TLS is enabled for the instance. | string | - | No |
| `labels` | The resource labels to represent user provided metadata. | map | {} | No |

## Outputs
| Name | Description |
|------|-------------|
| `id` | The memorystore instance ID. | 
| `host` | The IP address of the instance. | 
| `port` | The port number of the exposed Redis endpoint. | 
| `persistence_iam_identity` | Cloud IAM identity used by import/export operations. Format is 'serviceAccount:'. | 

## Prerequisites

For successful use of the module:

1. The version of project factory used in the project IaC should be at minimum version `v0.25.1`
2. The APIs which should be enabled in the project should be the following:
```
    "redis.googleapis.com",
    "iam.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
    "stackdriver.googleapis.com",
    "servicenetworking.googleapis.com",
    "cloudkms.googleapis.com",

```
3. The role that should be granted to the project resource editor in the Redis project is `roles/redis.admin`. This role has the minimum permissions needed to create the redis instance.
4. The project IaC should ensure that the following outputs are configured, these are necessary since the  module will pick up values directly from the output of the TFE project workspace.
* `project_id`
* `shared_vpc_project_id`
* `kms_crypto_key`
5. The minimum version that should be used for the `google` and `google-beta` providers in the workspace where the cluster is being created is `3.9.0`


## Development

### Requirements

You will need the following packages in order to develop locally:
* terraform v0.12.31
* inspec
